package com.yusp.mindist.tests;

import com.yusp.mindist.algo.MinDistCalculator;
import com.yusp.mindist.algo.PointN;
import com.yusp.mindist.io.InputFileParseFailedException;
import com.yusp.mindist.io.TsvReader;
import org.junit.Assert;
import org.junit.Test;

import java.util.List;

public class MinDistCalculatorTests {
    @Test
    public void sampleInput_2_8() {
        loadResourceAndCompare("sample_input_2_8.tsv", 3, 6);
    }

    @Test
    public void sampleInput_3_1000() {
        loadResourceAndCompare("sample_input_3_1000.tsv", 223, 388);
    }

    @Test
    public void sampleInput_4_4() {
        loadResourceAndCompare("sample_input_4_4.tsv", 2, 3);
    }

    @Test
    public void sampleInput_10_100() {
        loadResourceAndCompare("sample_input_10_100.tsv", 40, 94);
    }

    @Test
    public void sampleInput_100_100() {
        loadResourceAndCompare("sample_input_100_100.tsv", 48, 96);
    }

    private void loadResourceAndCompare(String resourceName, int idx1, int idx2) {
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource(resourceName).getPath();

        TsvReader reader = new TsvReader();
        try {
            List<PointN> points = reader.readFile(path);
            int[] results = MinDistCalculator.getClosestPoints(points);

            Assert.assertEquals(idx1, results[0]);
            Assert.assertEquals(idx2, results[1]);
        } catch (InputFileParseFailedException e) {
            Assert.fail();
        }
    }
}
