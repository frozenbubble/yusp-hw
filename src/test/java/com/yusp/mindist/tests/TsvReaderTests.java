package com.yusp.mindist.tests;

import com.yusp.mindist.algo.PointN;
import com.yusp.mindist.io.InputFileParseFailedException;
import com.yusp.mindist.io.TsvReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.io.FileNotFoundException;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class TsvReaderTests {
    @Test
    public void readerReadsPointsCorrectly() throws InputFileParseFailedException {
        ClassLoader classLoader = getClass().getClassLoader();
        String path = classLoader.getResource("sample_input_2_8.tsv").getPath();

        TsvReader reader = new TsvReader();
        List<PointN> points = reader.readFile(path);

        List<PointN> expectedPoints = new ArrayList<>();
        expectedPoints.add(new PointN(new double[] {-262972, 508697}));
        expectedPoints.add(new PointN(new double[] {-311943.65362731507, 370239.3559213022}));
        expectedPoints.add(new PointN(new double[] {742431, -772652}));
        expectedPoints.add(new PointN(new double[] {-346046, 696615.3537438104}));
        expectedPoints.add(new PointN(new double[] {194172, 103527}));
        expectedPoints.add(new PointN(new double[] {726621.8167057682, -813087.8844925504}));
        expectedPoints.add(new PointN(new double[] {167923, -312455.0459619701}));
        expectedPoints.add(new PointN(new double[] {499664.42762545496, 72395.09685360803}));

        for (int i = 0; i < expectedPoints.size(); i++) {
            Assert.assertEquals(expectedPoints.get(i).getCoordinates()[0], points.get(i).getCoordinates()[0], 0.01);
            Assert.assertEquals(expectedPoints.get(i).getCoordinates()[1], points.get(i).getCoordinates()[1], 0.01);

            Assert.assertEquals(2, points.get(i).getCoordinates().length);
        }
    }

    @Test
    public void fileNotFoundThrowsException() {
        try {
            ClassLoader classLoader = getClass().getClassLoader();
            File file = new File(classLoader.getResource("sample_input_2_8_malformed.tsv").getFile());

            String malformedInput = file.getAbsolutePath();
            TsvReader reader = new TsvReader();

            reader.readFile(malformedInput);
        } catch (InputFileParseFailedException e) {
            Assert.assertNull(e.getCause());

            return;
        }

        Assert.fail();
    }

    @Test
    public void pointsWithMisMatchingDimensionsThrowsException() {
        try {
            String homePath = System.getProperty("user.home");
            String randomFileName = UUID.randomUUID().toString();
            String outputPath = Paths.get(homePath, randomFileName).toAbsolutePath().toString();
            TsvReader reader = new TsvReader();

            reader.readFile(outputPath);
        } catch (InputFileParseFailedException e) {
            Assert.assertTrue(e.getCause() instanceof FileNotFoundException);

            return;
        }

        Assert.fail();
    }
}
