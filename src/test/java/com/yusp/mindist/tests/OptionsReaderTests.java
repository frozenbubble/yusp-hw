package com.yusp.mindist.tests;

import com.yusp.mindist.io.OptionsReader;
import org.junit.Assert;
import org.junit.Test;

import java.io.File;
import java.nio.file.Paths;

public class OptionsReaderTests {
    OptionsReader reader = new OptionsReader();
    String sampleInput;
    String sampleOutput;

    public OptionsReaderTests() {
        ClassLoader classLoader = getClass().getClassLoader();
        File file = new File(classLoader.getResource("sample_input_2_8.tsv").getFile());

        sampleInput = file.getAbsolutePath();

        String homePath = System.getProperty("user.home");
        sampleOutput = Paths.get(homePath, "output.txt").toAbsolutePath().toString();
    }

    @Test
    public void canParseValidInputs(){
        String[] args = new String[] {"-p", sampleInput, "-o", sampleOutput};

        Assert.assertTrue(reader.parse(args));
    }

    @Test
    public void missingInputPathIsNotAllowed(){
        String[] args = new String[] {"-o", sampleOutput};

        Assert.assertFalse(reader.parse(args));
    }

    @Test
    public void missingOutputPathIsNotAllowed(){
        String[] args = new String[] {"-p", sampleInput};

        Assert.assertFalse(reader.parse(args));
    }

    @Test
    public void inputFileMustExist(){
        String[] args = new String[] {"-p", "testfilename.test"};

        Assert.assertFalse(reader.parse(args));
    }
}
