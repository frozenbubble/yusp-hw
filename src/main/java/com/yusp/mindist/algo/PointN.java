package com.yusp.mindist.algo;

import lombok.Getter;

import java.util.stream.IntStream;

public class PointN {
    private double[] coordinates;

    public double[] getCoordinates(){
        return coordinates;
    }

    public PointN(double[] coordinates) {
        this.coordinates = coordinates;
    }

    public double distance(PointN other) {
        if (other.coordinates.length != coordinates.length) {
            throw new IllegalArgumentException("Dimensions must match");
        }

        double squareSum = IntStream.range(0, coordinates.length)
                .mapToDouble(i -> Math.pow((coordinates[i] - other.coordinates[i]), 2))
                .sum();

        return Math.sqrt(squareSum);
    }
}
