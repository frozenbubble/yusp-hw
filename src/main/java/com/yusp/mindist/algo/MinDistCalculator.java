package com.yusp.mindist.algo;

import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.DoubleStream;
import java.util.stream.Stream;

public class MinDistCalculator {
    private int idx1, idx2;
    private double d;

    public static int[] getClosestPoints(List<PointN> points) {
        int idx1 = 0;
        int idx2 = 1;
        double mindist = Double.MAX_VALUE;

        for(int i = 0; i < points.size(); i++){
            for (int j = i + 1; j < points.size(); j++) {
                double distance = points.get(i).distance(points.get(j));

                if (distance < mindist) {
                    idx1 = i;
                    idx2 = j;

                    mindist = distance;
                }
            }
        }

        return new int[] {idx1 + 1, idx2 + 1};
    }
//    public PointN[] getClosestPoints(List<PointN> points) {
////        Collections.sort(points, (p1, p2) -> (int)(p1.getCoordinates()[0] - p2.getCoordinates()[0]));
//        points.sort((p1, p2) -> (int)(p1.getCoordinates()[0] - p2.getCoordinates()[0]));
//
//
//        return null;
//    }

//    private List<PointN> divideAndFindClosest(List<PointN> points) {
//        if (points.size() < 2) {
//            throw new IllegalArgumentException("The list of points must contain at least two points");
//        }
//
//        if (points.size() == 2) {
//            return Arrays.asList(points.get(0), points.get(1));
//        }
//
//        double m1 = points.get(points.size()/2 - 1).getCoordinates()[0];
//        double m2 = points.get(points.size()/2).getCoordinates()[0];
//        double m = (m1+m2) / 2;
//
//        List<PointN> firstHalf = points.stream().filter(p -> p.getCoordinates()[0] < m).collect(Collectors.toList());
//        List<PointN> secondHalf = points.stream().filter(p -> p.getCoordinates()[1] >= m).collect(Collectors.toList());
//        List<PointN> closestLeft = divideAndFindClosest(firstHalf);
//        List<PointN> closestRight = divideAndFindClosest(secondHalf);
//
//        double minDistanceLeft = closestLeft.get(0).distance(closestLeft.get(1));
//        double minDistanceRight = closestRight.get(0).distance(closestLeft.get(1));
//        double delta = Math.min(minDistanceLeft, minDistanceRight);
//
//        List<PointN> deltaStripLeft = firstHalf.stream().filter(p -> Math.abs(m - p.getCoordinates()[0]) <= delta).collect(Collectors.toList());
//        List<PointN> deltaStripRight = secondHalf.stream().filter(p -> Math.abs(m - p.getCoordinates()[0]) <= delta).collect(Collectors.toList());
//
//        if (closestLeft.get(0).getCoordinates().length == 1) {
//            PointN rightMost = deltaStripLeft.get(deltaStripLeft.size() - 1);
//            PointN leftMost = deltaStripRight.get(deltaStripLeft.size() - 1);
//
//            if(rightMost.distance(leftMost) < delta){
//                return Arrays.asList(leftMost, rightMost);
//            } else if (minDistanceLeft < minDistanceRight) {
//                return closestLeft;
//            } else {
//                return closestRight;
//            }
//        } else {
//            List<PointN> projection = Stream.concat(deltaStripLeft.stream(), deltaStripRight.stream())
//                    .map(p -> {
//                        double[] newCoordinates = Arrays.stream(p.getCoordinates()).skip(1).toArray();
//                        return new PointN(newCoordinates);
//                    })
//                    .collect(Collectors.toList());
//
//
//        }

//        return null;
//    }
}
