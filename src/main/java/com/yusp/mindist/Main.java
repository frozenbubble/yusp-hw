package com.yusp.mindist;

import com.yusp.mindist.algo.MinDistCalculator;
import com.yusp.mindist.algo.PointN;
import com.yusp.mindist.io.*;

import java.util.List;

public class Main {
    public static void main(String[] args){
        OptionsReader optionsReader = new OptionsReader();

        if (!optionsReader.parse(args)){
            return;
        }

        try {
            PointListReader reader = new TsvReader();
            List<PointN> points = reader.readFile(optionsReader.getInputPath());
            int[] closest = MinDistCalculator.getClosestPoints(points);
            PointListWriter writer = new TextWriter();

            writer.writeToFile(optionsReader.getOutputPath(), points, closest);

        } catch (Exception e) {
            e.printStackTrace();
        }

    }
}
