package com.yusp.mindist.io;

import com.yusp.mindist.algo.PointN;
import org.apache.commons.csv.CSVFormat;
import org.apache.commons.csv.CSVRecord;

import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.Reader;
import java.util.ArrayList;
import java.util.List;

public class TsvReader implements PointListReader {
    @Override
    public List<PointN> readFile(String path) throws InputFileParseFailedException {
        try {
            Reader in = new FileReader(path);
            Iterable<CSVRecord> records = CSVFormat.TDF.parse(in);
            ArrayList<PointN> points = new ArrayList<>();
            int dimensionMismatches = -1;
            int numberofDimensions = 0;

            for (CSVRecord record : records) {
                int recordSize = record.size();

                if (recordSize != numberofDimensions) {
                    numberofDimensions = recordSize;
                    dimensionMismatches++;

                    if (dimensionMismatches > 0) {
                        throw new InputFileParseFailedException("Dimension mismatch in input file.");
                    }
                }

                double[] coordinates = new double[record.size()];

                for (int i = 0; i < record.size(); i++) {
                    coordinates[i] = Double.parseDouble(record.get(i));
                }

                PointN point = new PointN(coordinates);
                points.add(point);
            }

            return points;
        } catch (FileNotFoundException e) {
            throw new InputFileParseFailedException("Input file not found.", e);
        } catch (IOException e) {
            throw new InputFileParseFailedException("An IO error happened while parsing input file.", e);
        }

//        return null;
    }
}
