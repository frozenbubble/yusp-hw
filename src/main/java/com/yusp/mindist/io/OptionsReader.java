package com.yusp.mindist.io;

import org.apache.commons.cli.*;

import java.io.File;

public class OptionsReader {
    private String inputPath;
    private String outputPath;
    private Options options;

    public String getInputPath() {
        return inputPath;
    }

    public String getOutputPath() {
        return outputPath;
    }

    public OptionsReader() {
        options = new Options();
        options.addOption("p", true, "input path");
        options.addOption("o", true, "output path");
    }

    public boolean parse(String[] args){
        CommandLineParser parser = new DefaultParser();

        CommandLine cmd = null;
        try {
            cmd = parser.parse( options, args);
        } catch (ParseException e) {
            printUsage();

            return false;
        }

        if (!cmd.hasOption("p") || !cmd.hasOption("o")) {
            printUsage();

            return false;
        }

        inputPath = cmd.getOptionValue("p");
        outputPath = cmd.getOptionValue("o");
        File inputFile = new File(inputPath);
        File outputFile = new File(outputPath);

        if (!inputFile.exists() || inputFile.isDirectory()) {
            System.out.println("Input file does not exist.");

            return false;
        }

        if (!outputFile.getParentFile().canWrite()) {
            System.out.println("Cannot write to the specified output file.");

            return false;
        }

        return true;
    }

    private void printUsage() {
        System.out.println("Usage: mindist -p <input path> -o <output path>");
    }
}
