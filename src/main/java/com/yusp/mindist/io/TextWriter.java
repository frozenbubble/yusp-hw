package com.yusp.mindist.io;

import com.yusp.mindist.algo.PointN;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.List;

public class TextWriter implements PointListWriter {
    @Override
    public void writeToFile(String path, List<PointN> points, int[] indexes) throws WriteException, FileNotFoundException, UnsupportedEncodingException {
        PrintWriter writer = new PrintWriter(path, "UTF-8");

        try {
            for(int idx : indexes) {
                String line = idx + ":";

                for(double coord : points.get(idx).getCoordinates()) {
                    line += "\t" + coord;
                }

                writer.write(line);
            }

        } finally {
            writer.close();
        }
    }
}
