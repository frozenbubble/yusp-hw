package com.yusp.mindist.io;

import com.yusp.mindist.algo.PointN;

import java.util.List;

public interface PointListReader {
    List<PointN> readFile(String path) throws InputFileParseFailedException;
}
