package com.yusp.mindist.io;

public class InputFileParseFailedException extends Exception {
    public InputFileParseFailedException(){

    }

    public InputFileParseFailedException(String message) {
        super(message);
    }

    public InputFileParseFailedException(String message, Throwable cause) {
        super(message, cause);
    }

    public InputFileParseFailedException(Throwable cause) {
        super(cause);
    }
}
