package com.yusp.mindist.io;

import com.yusp.mindist.algo.PointN;

import java.io.FileNotFoundException;
import java.io.UnsupportedEncodingException;
import java.util.List;

public interface PointListWriter {
    void writeToFile(String path, List<PointN> points, int[] indexes) throws WriteException, FileNotFoundException, UnsupportedEncodingException;
}
